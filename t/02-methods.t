#!perl

use Test2::V0;
use Math::Lapack::Matrix;

my $a = Math::Lapack::Matrix->new(
				[
					[ 0,  1, 2],
					[ 3,  6, 7],
					[ 8,  9, 7],
					[ 3, -1, 4]
				]
);

my ($m, $n) = $a->shape;
is($m, 4, "Right number of rows");
is($n, 3, "Right number of rows");

my $max = $a->get_max;
_float($max, 9, "Right max");

my $min = $a->get_min;
_float($min, -1, "Right max");

my $mean = $a->mean();
_float($mean, 4.0833333, "Right mean");

my $s = $a->std_deviation();
_float($s, 3.2879486, "Right standard deviation");

$a->set_element(2,2,-1.33);
_float($a->get_element(2,2), -1.33, "Correct element after set the value");

my $matrix = Math::Lapack::Matrix->new(
    [
        [ 8, 5, 4, -3],
        [ 8, 9, 4, -3],
        [ 8, 4, 12, -3],
        [ 8, 5, 4, 15]
    ]
);
my $max_m = $matrix->max();

is($max_m->rows, 1, "Right number of rows");
is($max_m->columns, 4,"Right number of rows");
_float($max_m->get_element(0,0), 8, "Correct 0,0");
_float($max_m->get_element(0,1), 9, "Correct 0,1");
_float($max_m->get_element(0,2), 12, "Correct 0,2");
_float($max_m->get_element(0,3), 15, "Correct 0,3"); 



done_testing;


sub _float {
  my ($a, $b, $c) = @_;
  is($a, float($b, tolerance => 0.000001), $c);
}
