#!perl

use Test2::V0;

use Math::Lapack;
use Math::Lapack::Matrix;

my $A = Math::Lapack::Matrix->random(2,2);
my $B = Math::Lapack::Matrix->random(2,2);
my $C = $B->T;
my $D = $A x $C;

is($D->{args}[1]{type}, "transpose");

my $E = Math::Lapack::Expr::_optimize_ast($D);
ok(exists($D->{transpose_right}));

done_testing();
