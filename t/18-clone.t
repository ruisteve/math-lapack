#!perl

use Test2::V0 qw'is float done_testing';
use Math::Lapack::Matrix;

my $a = Math::Lapack::Matrix->new( 
				[ 
                    [1, 2, -10, 15, 17], 
					[3, 4, -12, 13, 1.777]
                ] 
);

my $m = $a->copy();

# Sum every element
is($m->rows, 2, "Get right number of rows");
is($m->columns, 5, "Get right number of columns");
_float($m->get_element(0,0), 1, "Element correct at 0,0");
_float($m->get_element(0,1), 2, "Element correct at 0,1");
_float($m->get_element(0,2), -10, "Element correct at 0,2");
_float($m->get_element(0,3), 15, "Element correct at 0,3");
_float($m->get_element(0,4), 17, "Element correct at 0,4");
_float($m->get_element(1,0), 3, "Element correct at 1,0");
_float($m->get_element(1,1), 4, "Element correct at 1,1");
_float($m->get_element(1,2), -12, "Element correct at 1,2");
_float($m->get_element(1,3), 13, "Element correct at 1,3");
_float($m->get_element(1,4), 1.777, "Element correct at 1,4");


done_testing;
sub _float {
  my ($a, $b, $c) = @_;
  is($a, float($b, tolerance => 0.000001), $c);
}
