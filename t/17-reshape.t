#!perl

use Test2::V0 qw'is float done_testing';
use Math::Lapack::Matrix;
use Math::Lapack::Expr;

my $a = Math::Lapack::Matrix->new( 
				[ [1, 2, -10, 15, 17], 
					[3, 4, -12, 13, 1.777]] 
);

my $b = Math::Lapack::Matrix->new(
				[
					[-4, -3, -2, -1, 0, 1, 2, 3, 4]
				]
);


# Sum every element
my $d = $a->reshape(10,1);
is($d->rows, 10, "Get right number of rows");
is($d->columns, 1, "Get right number of columns");
_float($d->get_element(0,0), 1, "Element correct at 0,0");
_float($d->get_element(1,0), 2, "Element correct at 0,0");
_float($d->get_element(2,0), -10, "Element correct at 0,0");
_float($d->get_element(3,0), 15, "Element correct at 0,0");
_float($d->get_element(4,0), 17, "Element correct at 0,0");
_float($d->get_element(5,0), 3, "Element correct at 0,0");
_float($d->get_element(6,0), 4, "Element correct at 0,0");
_float($d->get_element(7,0), -12, "Element correct at 0,0");
_float($d->get_element(8,0), 13, "Element correct at 0,0");
_float($d->get_element(9,0), 1.777, "Element correct at 0,0");

my $c = $b->frobenius_norm();
_float($c, 7.745966692414834, "Right frobenius norm");


done_testing;
sub _float {
  my ($a, $b, $c) = @_;
  is($a, float($b, tolerance => 0.000001), $c);
}
