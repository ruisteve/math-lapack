package Math::Lapack::Expr;

use v5.14;

use warnings;
use strict;
use strictures 2;
use Scalar::Util qw'blessed refaddr';
use Data::Dumper;
use parent 'Exporter';
our $_debug_counts;

our @EXPORT = qw.$_debug_counts transpose inverse sum abs T.;

use overload
  '0+' => \&eval_ast,
  '""' => \&to_string,
  '**' => \&pow_ast,
  '-'  => \&sub_ast,
  '+'  => \&add_ast,
  '*'  => \&mul_ast,
  '/'  => \&div_ast,
  'x'  => \&dot_ast,
  log  => \&log_ast,
  exp  => \&exp_ast;

our $AUTOLOAD;


=pod

=for Pod::Coverage T add_ast div_ast dot_ast eval_ast exp_ast log_ast mul_ast pow_ast sub_ast sum to_string transpose inverse

=cut



sub to_string {
  &eval_ast
}

sub add_ast {
	my ($a, $b) = @_;
	return bless { type => 'add', args => [$a,$b] } => __PACKAGE__;
}

sub sub_ast {
	my ($a, $b, $s) = @_;
	return bless { type => 'sub', args =>  [$a,$b,$s] } => __PACKAGE__;
}

sub mul_ast {
	my ($a, $b) = @_;
	return bless { type => 'mul', args => [$a, $b] } => __PACKAGE__;
}

sub div_ast {
	my ($a, $b, $s) = @_;
	return bless { type => 'div', args => [$a,$b,$s] } => __PACKAGE__;
}

sub dot_ast {
	my ($a, $b) = @_;
	return bless { type => 'dot', args => [$a, $b] } => __PACKAGE__;
}

sub T { &transpose }
sub transpose {
	my $a = shift;
	return bless { type => 'transpose', args => [$a] } => __PACKAGE__;
}

sub inverse {
	my $a = shift;
	return bless { type => 'inverse', args => [$a] } => __PACKAGE__;
}

sub pow_ast {
	my ($a, $b) = @_;
	return bless { type => 'pow', args => [$a, $b] } => __PACKAGE__;
}

sub sum {
	my ($a, $b) = @_;
	return bless { type => 'sum', args => [$a, $b] } => __PACKAGE__;
}

sub log_ast {
	my ($a) = @_;
	return bless { type => 'log', args => [$a] } => __PACKAGE__;
}

sub exp_ast {
	my ($a) = @_;
	return bless { type => 'exp', args => [$a] } => __PACKAGE__;
}

## Special cases (for now)
our %evaluators = (
                   dot => sub {
                     my $tree = shift;
                     return Math::Lapack::Matrix::eval_dot($tree->{args}[0],
                                                           $tree->{args}[1],
                                                           $tree->{transpose_left},
                                                           $tree->{transpose_right});
                   }
                  );

sub eval_ast {
    my $tree = shift;

    $_debug_counts->{eval_ast}++;

    if(blessed($tree) && $tree->isa(__PACKAGE__)) { # Is this an Expr?
        return $tree->{evaluated} if exists($tree->{evaluated}); # return it if we did evaluate it already

        if ($tree->{type} ne "matrix") {
            my $evaluated = exists($tree->{simplified}) ? $tree->{simplified} : _optimize_ast($tree);

            if (exists($tree->{args})) {
                $tree->{args} = [ map {eval_ast($_)} @{$tree->{args}} ];
            }

            my $ans;
            if (exists($evaluators{$tree->{type}})) {
                $ans = $evaluators{$tree->{type}}->($tree);
            }
            else {
                no strict 'refs';
                my $package = exists($tree->{package}) ? $tree->{package} : "Math::Lapack::Matrix";
                # print STDERR "Calling $package with eval_$tree->{type}\n\n";  ## DEBUG
                $ans = "${package}::eval_$tree->{type}"->(@{$tree->{args}});
            }
            $tree->{evaluated} = $ans;
            return $ans;
        }
    }
    return $tree;
}


sub _is_transpose {
  my $child = shift;
  return blessed($child) && $child->isa(__PACKAGE__) && $child->{type} eq "transpose";
}

sub _optimize_ast {

    my ($tree) = @_;

    $_debug_counts->{optimize}++;

    my @to_process = (\$tree);

    my %visited = ();
    while (@to_process) {

        my $first = shift @to_process;

        if (blessed($$first) && $$first->isa(__PACKAGE__)) {
            $visited{refaddr($$first)}++;

            if (exists($$first->{evaluated})) {
                $$first = $$first->{evaluated};

            } elsif (exists($$first->{simplified})) {
                $$first = $$first->{simplified};

            } elsif (exists($$first->{args})) {  ## Autovivification is a thing
                my @child = @{$$first->{args}};

                if ($visited{refaddr($$first)} > 1) { # we already processed its childs
                    ## check if we are a dot
                    if ($$first->{type} eq "dot") {
                        ## Dot has just two children

                        ## is the left side transposed?
                        if (_is_transpose($child[0])) {
                            ## save flag
                            $$first->{transpose_left} = 1;
                            ## take the transpose child, and make it ours
                            $$first->{args}[0] = $child[0]{args}[0];
                        }

                        if (_is_transpose($child[1])) {
                            ## save flag
                            $$first->{transpose_right} = 1;
                            ## take the transpose child, and make it ours
                            $$first->{args}[1] = $child[1]{args}[0];
                        }
                    }

                } else {
                    unshift @to_process, $first;  # put father back in
                    unshift @to_process, map { \$_ } @{$$first->{args}};
                }
            }
        }
        elsif (blessed($$first) && ref($$first) !~ /Matrix/) {
            die ref($first)
        }
    }

    return $tree
}

sub DESTROY {
  my $self = shift;
  ## nothing for now
}

AUTOLOAD {
  my $method = $AUTOLOAD;

#  our $Depth //= 0;
  state $Depth ;
  $Depth++;

  $method =~ s/.*:://;  # transform Math::Lapack::Expr::get_element into get_element (for example)
  my $obj = shift;      # get object where unknown method was called

  if (blessed($obj)) {
    my $evaluated_tree = $obj->eval_ast;

  if ($Depth > 10_000_000) {
     my $ref = ref($obj);
      my $ref_tree = ref($evaluated_tree);
       die <<"EOD"
 *** AUTOLOAD[$method] in deep recursion.
 *** OBJ is a $ref.
 *** TREE is a $ref_tree.
EOD
   }

    if (blessed($evaluated_tree)) {
      return $evaluated_tree->$method(@_); # evaluate the object, and invoke method on resulting value
    } else {
      no strict 'refs';
      return &$method($evaluated_tree);
    }
  } else {
    return $obj;
  }
}

1;
