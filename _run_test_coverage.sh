dzil build
cd Math-Lapack-*
perl Build.PL
cover -test -report html -make Build -outputdir ../coverage_report
cd ..
xdg-open coverage_report/coverage.html
